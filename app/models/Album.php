<?php

class Album extends Eloquent {

	/**
	* Return albums artist
	*
	* @return Album
	*/
	public function artist(){
		return $this->belongsTo('Artist');
	}

}
