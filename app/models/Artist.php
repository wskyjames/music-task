<?php

class Artist extends Eloquent {

	/**
	* Return all artist albums
	*
	* @return Album
	*/
	public function albums(){
		return $this->hasMany('Album');
	}

}
