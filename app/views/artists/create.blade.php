<html>
<head>
    <title>My Music</title>
    <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('/')}}">Home</a>
    </div>
	<ul class="nav navbar-nav">
        <li><a href="{{ URL::to('artists') }}">Artists</a></li>
		<li><a href="{{ URL::to('albums') }}">Albums</a></li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			Top Ten <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="{{ URL::to('search/top_ten/artists') }}">Artists</a></li>
				<li><a href="{{ URL::to('search/top_ten/albums') }}">Albums</a></li>
			</ul>
		</li>
    </ul>
	<div class="col-sm-3 col-md-3 pull-right">
	{{ Form::open(array('action' => 'SearchController@searchAll')) }}
        <div class="form-group">
            <input type="text" class="form-control" style="margin-top:5px;" placeholder="Search" name="search" id="srch-term">            
        </div>
	{{ Form::close() }}
	</div>	
</nav>

<h1>Add new Artist</h1>
<div class="row">
	<div class="col-md-5">
	
		{{ HTML::ul($errors->all()) }}

		{{ Form::open(array('url' => 'artists')) }}

		<div class="form-group">
			{{ Form::label('name', 'Name') }}
			{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
		</div>

		<div class="form-group">
			{{ Form::label('genre', 'Genre') }}
			{{ Form::text('genre', Input::old('genre'), array('class' => 'form-control')) }}
		</div>
   
		{{ Form::submit('Add New Artist', array('class' => 'btn btn-primary')) }}

		{{ Form::close() }}
	
	</div>
</div>

</div>

<!-- jQuery 1.11.1 - provides ie8 support -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- Botstrap JS -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

</body>
</html>
