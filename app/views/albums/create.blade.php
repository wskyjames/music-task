<html>
<head>
    <title>My Music</title>
     <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- Bootstrap select css -->
    <link href="{{ URL::asset('css/bootstrap-select.min.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('/')}}">Home</a>
    </div>
	 <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('artists') }}">Artists</a></li>
		<li><a href="{{ URL::to('albums') }}">Albums</a></li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			Top Ten <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="{{ URL::to('search/top_ten/artists') }}">Artists</a></li>
				<li><a href="{{ URL::to('search/top_ten/albums') }}">Albums</a></li>
			</ul>
		</li>
    </ul>	
	<div class="col-sm-3 col-md-3 pull-right">
	{{ Form::open(array('action' => 'SearchController@searchAll')) }}
        <div class="form-group">
            <input type="text" class="form-control" style="margin-top:5px;" placeholder="Search" name="search" id="srch-term">            
        </div>
	{{ Form::close() }}
	</div>
	
</nav>

<h1>Add an album</h1>

<div class="row">
	<div class="col-md-5">
	
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'albums')) }}

	<div class="form-group">
        {{ Form::label('artist', 'Arist') }}
		<br />
        {{ Form::select('artist', $artists, array('class' => 'selectartist')) }}
		<br /><br />
		<a class="btn btn-small btn-success" href="{{ URL::to('artists/create') }}">Add New Artist</a>
    </div>

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('sold', 'Total Albums Sold') }}
        {{ Form::number('sold', Input::old('sold'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Add album!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

	</div>
</div>

</div>

<!-- jQuery 1.11.1 - provides ie8 support -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- Bootstrap select html -->
<script src="{{ URL::asset('js/bootstrap-select.min.js') }}"></script>
<!-- Add reference for select statement -->
<script>
	$('.selectartist').selectpicker();
</script>

</body>
</html>
