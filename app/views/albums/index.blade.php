<html>
<head>
    <title>My Music</title>
    <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('/')}}">Home</a>
    </div>
	<ul class="nav navbar-nav">
        <li><a href="{{ URL::to('artists') }}">Artists</a></li>
		<li><a href="{{ URL::to('albums') }}">Albums</a></li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			Top Ten <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="{{ URL::to('search/top_ten/artists') }}">Artists</a></li>
				<li><a href="{{ URL::to('search/top_ten/albums') }}">Albums</a></li>
			</ul>
		</li>
    </ul>
	<div class="col-sm-3 col-md-3 pull-right">
	{{ Form::open(array('action' => 'SearchController@searchAll')) }}
        <div class="form-group">
            <input type="text" class="form-control" style="margin-top:5px;" placeholder="Search" name="search" id="srch-term">            
        </div>
	{{ Form::close() }}
	</div>
	
</nav>

<h1>All albums</h1>

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<a class="btn btn-small btn-success" href="{{ URL::to('albums/create') }}">Add New Album</a>
<br /><br />

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
			<td>Artist</td>
            <td>Copies Sold</td>
        </tr>
    </thead>
    <tbody>
	 @foreach($albums as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->name }}</td>
			<td>{{ $value->artist->name }}</td>
			<td>{{ $value->sold }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>

<!-- jQuery 1.11.1 - provides ie8 support -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- Botstrap JS -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

</body>
</html>
