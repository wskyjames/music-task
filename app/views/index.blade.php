<html>
<head>
    <title>My Music</title>
    <!-- Bootstrap -->    
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('/')}}">Home</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('artists') }}">Artists</a></li>
		<li><a href="{{ URL::to('albums') }}">Albums</a></li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			Top Ten <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="{{ URL::to('search/top_ten/artists') }}">Artists</a></li>
				<li><a href="{{ URL::to('search/top_ten/albums') }}">Albums</a></li>
			</ul>
		</li>
    </ul>	
	<div class="col-sm-3 col-md-3 pull-right">
	{{ Form::open(array('action' => 'SearchController@searchAll')) }}
        <div class="form-group">
            <input type="text" class="form-control" style="margin-top:5px;" placeholder="Search" name="search" id="srch-term">            
        </div>
	{{ Form::close() }}
	</div>
</nav>

<div class="jumbotron">
  <h1>Hello Lee</h1>
  <p>
	The site contains all of the features outlined in the task document. I was contemplating making
    some of the features, such as search, with angular. I can provide this if you like, however it
	is straight Laravel at the moment.
	<br /><br />
	I didn't think Unit Tests were within the scope, but once again, i can provide some if you like.
	Also, as i've used a route to resource with artists and albums I could easily add the edit/delete 
	functionality to both -- thought it best to stick with the resume though.
	<br /><br />
	Look forward to hearing from you.
  </p>
</div>

</div>

<!-- jQuery 1.11.1 - provides ie8 support -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- Botstrap JS -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

</body>
</html>
