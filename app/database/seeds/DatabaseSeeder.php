<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('ArtistsTableSeeder');
		$this->command->info('Artist table seed complete.');
		$this->call('AlbumsTableSeeder');
		$this->command->info('Album table seed complete.');
	}

}
