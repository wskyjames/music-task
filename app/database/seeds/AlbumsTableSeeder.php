<?php

class AlbumsTableSeeder extends Seeder 
{

	public function run()
	{
		DB::table('albums')->delete();

		Album::create(array(
			'artist_id' => 1,
			'name'      => 'The Bends',
			'sold'      => '1000000'
		));
		
		Album::create(array(
			'artist_id' => 1,
			'name'      => 'Ok Computer',
			'sold'      => '2000000'
		));
		
		Album::create(array(
			'artist_id' => 1,
			'name'      => 'Kid A',
			'sold'      => '100000'
		));
				
		Album::create(array(
			'artist_id' => 2,
			'name'      => 'Led Zeppelin',
			'sold'      => '750000'
		));
		
		Album::create(array(
			'artist_id' => 2,
			'name'      => 'Led Zeppelin II',
			'sold'      => '1200000'
		));
		
		Album::create(array(
			'artist_id' => 2,
			'name'      => 'Led Zeppelin III',
			'sold'      => '3800000'
		));
		
		Album::create(array(
			'artist_id' => 3,
			'name'      => 'Whatever People Say I Am, That`s What I`m Not',
			'sold'      => '8800000'
		));
		
		Album::create(array(
			'artist_id' => 3,
			'name'      => 'Favourite Worst Nightmare',
			'sold'      => '5800000'
		));
		
		Album::create(array(
			'artist_id' => 3,
			'name'      => 'Humbug',
			'sold'      => '1500000'
		));

		Album::create(array(
			'artist_id' => 4,
			'name'      => 'Slowhand',
			'sold'      => '156000'
		));
		
		Album::create(array(
			'artist_id' => 4,
			'name'      => '461 Ocean Boulevard',
			'sold'      => '350000'
		));
		
		Album::create(array(
			'artist_id' => 4,
			'name'      => 'Crossroads',
			'sold'      => '120000'
		));
				
		Album::create(array(
			'artist_id' => 5,
			'name'      => 'My Generation',
			'sold'      => '5370000'
		));
		
		Album::create(array(
			'artist_id' => 5,
			'name'      => 'Tommy',
			'sold'      => '7480000'
		));
		
		Album::create(array(
			'artist_id' => 5,
			'name'      => 'Who`s Next',
			'sold'      => '630000'
		));
		
		Album::create(array(
			'artist_id' => 6,
			'name'      => 'Jimi Hendrix Experience',
			'sold'      => '5340000'
		));
		
		Album::create(array(
			'artist_id' => 6,
			'name'      => 'Band Of Gypsies',
			'sold'      => '2990000'
		));
		
		Album::create(array(
			'artist_id' => 6,
			'name'      => 'Voodoo Child',
			'sold'      => '720000'
		));
		
		Album::create(array(
			'artist_id' => 7,
			'name'      => 'After The Gold Rush',
			'sold'      => '940000'
		));
		
		Album::create(array(
			'artist_id' => 7,
			'name'      => 'Harvest',
			'sold'      => '3690000'
		));
		
		Album::create(array(
			'artist_id' => 7,
			'name'      => 'Rust Never Sleeps',
			'sold'      => '260000'
		));
		
		Album::create(array(
			'artist_id' => 8,
			'name'      => 'Dark Side Of The Moon',
			'sold'      => '735000'
		));
		
		Album::create(array(
			'artist_id' => 8,
			'name'      => 'Another Brick In The Wall',
			'sold'      => '2690000'
		));
		
		Album::create(array(
			'artist_id' => 8,
			'name'      => 'Wish You Were Here',
			'sold'      => '9830000'
		));
		
		Album::create(array(
			'artist_id' => 9,
			'name'      => 'Abby Road',
			'sold'      => '2335000'
		));
		
		Album::create(array(
			'artist_id' => 9,
			'name'      => 'Let It Be',
			'sold'      => '274690000'
		));
		
		Album::create(array(
			'artist_id' => 9,
			'name'      => 'Revolver',
			'sold'      => '97830000'
		));
		
		Album::create(array(
			'artist_id' => 10,
			'name'      => 'GRRR',
			'sold'      => '2285000'
		));
		
		Album::create(array(
			'artist_id' => 10,
			'name'      => 'Sticky Fingers',
			'sold'      => '2680000'
		));
		
		Album::create(array(
			'artist_id' => 10,
			'name'      => 'Some Girls',
			'sold'      => '6330000'
		));
		
		Album::create(array(
			'artist_id' => 11,
			'name'      => 'News Of The World',
			'sold'      => '2237500'
		));
		
		Album::create(array(
			'artist_id' => 11,
			'name'      => 'A Kind Of Magic',
			'sold'      => '2687300'
		));
		
		Album::create(array(
			'artist_id' => 11,
			'name'      => 'Hot Space',
			'sold'      => '7458000'
		));
		
		Album::create(array(
			'artist_id' => 12,
			'name'      => 'Back In Black',
			'sold'      => '227500'
		));
		
		Album::create(array(
			'artist_id' => 12,
			'name'      => 'Highway To Hell',
			'sold'      => '2693300'
		));
		
		Album::create(array(
			'artist_id' => 12,
			'name'      => 'T.N.T',
			'sold'      => '9658000'
		));
		
	}

}
