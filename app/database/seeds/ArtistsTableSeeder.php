<?php

class ArtistsTableSeeder extends Seeder 
{

	public function run()
	{
		DB::table('artists')->delete();

		Artist::create(array(
			'name' => 'Radiohead',
			'genre' => 'Prog Rock'
		));

		Artist::create(array(
			'name' => 'Led Zeppelin',
			'genre' => 'Rock'
		));

		Artist::create(array(
			'name' => 'Arctic Monkeys',
			'genre' => 'Rock'
		));
		
		Artist::create(array(
			'name' => 'Eric Clapton',
			'genre' => 'Blues'
		));
						
		Artist::create(array(
			'name' => 'The Who',
			'genre' => 'Rock'
		));
		
		Artist::create(array(
			'name' => 'Jimi Hendrix',
			'genre' => 'Rock'
		));
		
		Artist::create(array(
			'name' => 'Neil Young',
			'genre' => 'Folk'
		));
		
		Artist::create(array(
			'name' => 'Pink Floyd',
			'genre' => 'Rock'
		));
		
		Artist::create(array(
			'name' => 'The Beatles',
			'genre' => 'Rock'
		));
		
		Artist::create(array(
			'name' => 'The Rolling Stones',
			'genre' => 'Rock'
		));
		
		Artist::create(array(
			'name' => 'Queen',
			'genre' => 'Rock'
		));
		
		Artist::create(array(
			'name' => 'AC/DC',
			'genre' => 'Rock'
		));
		
	}

}
