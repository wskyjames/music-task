<?php

/*
|-----------------------------------
| Application routing
|-----------------------------------
|*/

/**
* Home screen -- default to the artists screen
*/
Route::get('/', function()
{
	return View::make('index');
});

/*

/**
* Artists Resource
*/
Route::resource('artists', 'ArtistController');
	
/**
* Albums resource
*/
Route::resource('albums', 'AlbumController');
	
/**
* Search controller
*/
Route::post('search/{search}', 'SearchController@searchAll');
Route::get('search/artist_albums/{artist_id}', 'SearchController@searchArtistAlbums');
Route::get('search/top_ten/albums', 'SearchController@searchPopularAlbums');
Route::get('search/top_ten/artists', 'SearchController@searchPopularArtists');

/**
* Catch missing routes and redirect to index
*/
App::missing(function($exception)
{
	return View::make('index');
});


