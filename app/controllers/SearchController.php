<?php
class SearchController extends BaseController {

	/**
     * Display all artist albums
     *
     * @return View
     */
    public function searchArtistAlbums($artist_id){
        // get all the artists
        $artist = Artist::find($artist_id);
        // load the view and pass the artist and their albums
        return View::make('artists.albums')
                    ->with(array('artist' => $artist,
			                     'albums' => $artist->albums));
    }
	
	/**
	* Search albums and artists
	*
	* @return View
	*/
	public function searchAll($search){
		// return arrays
		$artists = array();
		$albums  = array();
		// get search term
		$search = Input::get('search');
		// split words for search term
		$search_terms = explode(' ', $search);
		// set table for artists
		$artist_query = DB::table('artists');
		$album_query  = DB::table('albums');
		// loop all 
		foreach($search_terms as $term){
			// bind each term
			$artist_query->where('name', 'LIKE', '%'. $term .'%');
			$album_query->where('name', 'LIKE', '%'. $term .'%');
		}
		// fetch data
		$artists = $artist_query->orderBy('name', 'asc')
					            ->get();
		$albums  = $album_query->orderBy('name', 'asc')
					            ->get();
		// return data to view
		return View::make('search.index')->with(array('artists' => $artists,
		                                              'albums'  => $albums));
	}
	
	/**
	* Return top ten albums by copies sold
	*
	* @return View
	*/
	public function searchPopularAlbums(){
		// fetch top ten albums sold
		$albums = DB::table('albums')					
					->select(DB::raw('albums.id, artists.name  as artist_name, albums.name, albums.sold'))
					->join('artists', 'albums.artist_id', '=', 'artists.id')
					->orderBy('albums.sold', 'desc')
					->take(10)
					->get();
		// return data to view
		return View::make('albums.top')->with('albums', $albums);
	}
	
	/**
	* Return top ten artists by combined album sales
	*
	* @return View
	*/
	public function searchPopularArtists(){
		// join artists with album and get total sold
		$artists = DB::table('artists')					
					->select(DB::raw('artists.id, artists.name, SUM(albums.sold) as total'))
					->join('albums', 'albums.artist_id', '=', 'artists.id')
					->groupBy('artists.id')
					->orderBy('total', 'desc')
					->take(10)
					->get();
		// return data to view
		return View::make('artists.top')->with('artists', $artists);		
	}
	
}
