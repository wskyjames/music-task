<?php
class AlbumController extends BaseController {
	
	/**
     * Display all albums
     *
     * @return View
     */
    public function index(){
        // get all the albums
        $albums = Album::all();
        // load the view and pass the album
        return View::make('albums.index')->with('albums', $albums);
    }
	
	/**
     * Form for creating new album
     *
     * @return View
     */
    public function create(){
		// fetch artists in list friendly format for html select
		$artists = Artist::lists('name', 'id');
		// return view
        return View::make('albums.create')->with('artists', $artists);
    }
	
	/**
    * Add a new album
    *
    * @return View
    */
    public function store(){
        // check all data is submitted
        $rules = array(
			'artist' => 'required',
            'name'   => 'required',
            'sold'   => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);		
		// if data is missing return to view
        if ($validator->fails()) {
            return Redirect::to('albums/create')
                ->withErrors($validator)
                ->withInput();
        } else {
			// fetch artist
			$artist     = Artist::find(Input::get('artist'));
			$check_size = sizeof($artist);
			// check for valid artist
			if($check_size > 0){
				// add artist to db
				$album = new Album;
				$album->name = Input::get('name');
				$album->sold = Input::get('sold');
				// save the album through artist relationship
				$artist->albums()->save($album);
				// redirect
				Session::flash('message', 'Album has been added');
				return View::make('albums.index')->with('albums', Album::all());
			}else{
				return Redirect::to('albums/create')
					->withErrors(Artist::find(Input::get('artist'))->count)
					->withInput();
			}
        }
    }

	
}
