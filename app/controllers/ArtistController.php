<?php

class ArtistController extends BaseController {
	
	/**
     * Display a list of artists
     *
     * @return View
     */
    public function index(){
        // get all artists
        $artists = Artist::all();
        // pass the artist list to the view
        return View::make('artists.index')->with('artists', $artists);
    }
	
	/**
    * Form for creating new artist's
    *
    * @return View
    */
    public function create(){
        return View::make('artists.create');
    }
	
	/**
    * Add a new artist
    *
    * @return View
    */
    public function store(){
        // check all data is submitted
        $rules = array(
            'name'       => 'required',
            'genre'      => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        // if data is missing return to view
        if ($validator->fails()) {
            return Redirect::to('artists/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // add artist to db
            $artist = new Artist;
			$artist->name  = Input::get('name');
			$artist->genre = Input::get('genre');
			$artist->save();
            // redirect
            Session::flash('message', 'Artist has been added');
            return View::make('artists.index')->with('artists', Artist::all());
        }
    }



	
}
