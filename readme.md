## Music Task

A small project created using Laravel and Bootstrap. It contains a music and album store with features to display the artist/album, search through the archives and list top ten artists/albums based on (fictitious) album sale figures.

## Download

Download the package manually or through git:

```
#!php

git clone https://bitbucket.org/wskyjames/music-task.git

```

## Install dependencies

Change into the application directory then pull down all vendor dependencies:

```
#!php

cd {www/app_dir/}
composer install

```


## Migrating and Seeding

Change the database settings found in app/config/database.php to contain your username, pass and database name.

Then run the commands below:

```
#!php

php artisan migrate
php artisan db:seed

```


